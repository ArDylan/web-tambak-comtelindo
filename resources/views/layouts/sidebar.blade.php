<!-- Start Header menu area -->
<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="/"><img class="main-logo" src="{{asset('img/logo-comtel.png')}}" alt="" style="width: 200px; height: 60px" /></a>
            {{-- <a href="/">Tambak</a> --}}
            <strong><a href="/"><img src="{{asset('img/logo.jpg')}}" alt="" style="width: 45px; height: 38px" /></a></strong>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar" style="margin-top: 40px">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li>
                        <a title="Beranda" href="/" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Beranda</span></a>
                    </li>
                    <li>
                        <a title="Atur Temperatur" href="/atur-suhu" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Atur Temperatur</span></a>
                    </li>
                    <li>
                        <a title="Tambak" href="/tambak" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Tambak</span></a>
                    </li>
                    <li>
                        <a title="Riwayat" href="/riwayat" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Tabel Riwayat</span></a>
                    </li>
                </ul>
            </nav>
        </div>
    </nav>
</div>
<!-- End Header menu area -->