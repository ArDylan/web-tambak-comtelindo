<!doctype html>
<html class="no-js" lang="">

<head>
   @include('layouts.head')
   @yield('head-link')
</head>
<body>
    @include('layouts.sidebar')
    @include('layouts.header')

    @yield('content')

    @include('layouts.footer')

    @yield('script')
</body>

</body>

</html>
