    <div class="footer-copyright-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2021 Archie Dylan. All rights reserved.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <!-- jquery
    ============================================ -->
    <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- wow JS
        ============================================ -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="{{asset('assets/js/jquery-price-slider.js')}}"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!-- sticky JS
        ============================================ -->
    <script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="{{asset('assets/js/jquery.scrollUp.min.js')}}"></script>
    <!-- counterup JS
        ============================================ -->
    <script src="{{asset('assets/js/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/js/counterup/waypoints.min.js')}}"></script>
    <script src="{{asset('assets/js/counterup/counterup-active.js')}}"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="{{asset('assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{asset('assets/js/scrollbar/mCustomScrollbar-active.js')}}"></script>
    <!-- metisMenu JS
        ============================================ -->
    <script src="{{asset('assets/js/metisMenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/js/metisMenu/metisMenu-active.js')}}"></script>
    <!-- data table JS
		============================================ -->
        <script src="{{asset('assets/js/data-table/bootstrap-table.js')}}"></script>
        <script src="{{asset('assets/js/data-table/tableExport.js')}}"></script>
        <script src="{{asset('assets/js/data-table/data-table-active.js')}}"></script>
        <script src="{{asset('assets/js/data-table/bootstrap-table-editable.js')}}"></script>
        <script src="{{asset('assets/js/data-table/bootstrap-editable.js')}}"></script>
        <script src="{{asset('assets/js/data-table/bootstrap-table-resizable.js')}}"></script>
        <script src="{{asset('assets/js/data-table/colResizable-1.5.source.js')}}"></script>
        <script src="{{asset('assets/js/data-table/bootstrap-table-export.js')}}"></script>
    <!--  editable JS
		============================================ -->
    <script src="{{asset('assets/js/editable/jquery.mockjax.js')}}"></script>
    <script src="{{asset('assets/js/editable/mock-active.js')}}"></script>
    <script src="{{asset('assets/js/editable/select2.js')}}"></script>
    <script src="{{asset('assets/js/editable/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/editable/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{asset('assets/js/editable/bootstrap-editable.js')}}"></script>
    <script src="{{asset('assets/js/editable/xediable-active.js')}}"></script>
    <!-- calendar JS
        ============================================ -->
    <script src="{{asset('assets/js/calendar/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/calendar/fullcalendar.min.js')}}"></script>
    <script src="{{asset('assets/js/calendar/fullcalendar-active.js')}}"></script>
    <!-- dropzone JS
    ============================================ -->
    <script src="{{asset('assets/js/dropzone/dropzone.js')}}"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <!-- main JS
        ============================================ -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <!-- tawk chat JS
        ============================================ -->
    {{-- <script src="{{asset('assets/js/tawk-chat.js')}}"></script> --}}
    <!-- chosen JS
    ============================================ -->
    <script src="{{asset('assets/js/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('assets/js/chosen/chosen-active.js')}}"></script>
    <!-- select2 JS
        ============================================ -->
    <script src="{{asset('assets/js/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2/select2-active.js')}}"></script>

        <!-- tab JS
		============================================ -->
        <script src="{{asset('assets/js/tab.js')}}"></script>

        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
