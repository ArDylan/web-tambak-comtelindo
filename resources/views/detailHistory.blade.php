@extends('layouts.app')
@section('content')
        <!-- Static Table Start -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <form action="/riwayat/export" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{$farm->id}}" name="farm_id">
                                <div class="main-sparkline13-hd">
                                    <h1>Riwayat Tambak {{$farm->name}}</h1>
                                    <button type="submit" class="btn btn-primary">Export</button>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <div class="white-box analytics-info-cs">
                                            {{-- <h3 class="box-title">Cari Tambak</h3> --}}
                                        <div class="sparkline10-graph">
                                            <div class="input-knob-dial-wrap">
                                                <div class="row">
                                                     
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <div class="mg-b-20">
                                                                    <label>Tanggal Awal</label>
                                                                    <input type="date" class="form-control" id="startDate" name="startDate"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <div class=" mg-b-20">
                                                                    <label>Tanggal Akhir</label>
                                                                    <input type="date" class="form-control" id="endDate" name="endDate"/>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class=" mg-b-20">
                                                                    <button class="btn btn-primary" id="dateButton">Search</button>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table id="table-history" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th class="sorting" data-field="name" data-editable="true">tanggal</th>
                                                <th class="sorting" data-field="name" data-editable="true">jam</th>
                                                <th class="sorting" data-field="name" data-editable="true">ph</th>
                                                <th class="sorting" data-field="name" data-editable="true">temperatur</th>
                                                <th class="sorting" data-field="name" data-editable="true">do</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
@endsection


@section('script')

<script>

    $.ajaxSetup({
        headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content"),
            }
        })

</script>

<script>

$(document).ready(function(){
    var historyTable =  $('#table-history').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    responsive: true,
    ajax: {
        type: "GET",
        url: "/riwayat/{{$farm->id}}",
        data: {tanggal1 : "{{$tanggal1}}", tanggal2 : "{{$tanggal2}}"},

    },
    columns: [
        { data: 'date', name: 'date'},
        { data: 'time', name: 'time'},
        { data: 'ph', name: 'ph'},
        { data: 'temp', name: 'temp'},
        { data: 'do', name: 'do'},
    ],
    });
    $('#dateButton').click(function() {
        var tanggal1 = $('#startDate').val();
        var tanggal2 = $('#endDate').val();

        historyTable.destroy()



        historyTable = $('#table-history').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        responsive: true,
        ajax: {
            type: "GET",
            url: "/riwayat/{{$farm->id}}",
            data: {
                data1:tanggal1,
                data2:tanggal2,
            },
        },
        columns: [
            { data: 'date', name: 'date'},
            { data: 'time', name: 'time'},
            { data: 'ph', name: 'ph'},
            { data: 'temp', name: 'temp'},
            { data: 'do', name: 'do'},
        ],
        });
    });
});


</script>

@endsection