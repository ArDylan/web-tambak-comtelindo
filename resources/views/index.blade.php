@extends('layouts.app')
@section('content')


        {{-- statistik atas --}}
        <div class="traffice-source-area mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info-cs">
                            <h3 class="box-title">PH Tambak</h3>
                            <ul class="list-inline two-part-sp">
                                <li>  @if($farms->max('ph') > $ph->maximum) {{$farms->where('ph', $farms->max('ph'))->first()->farm->name}} @endif </li>
                                <li class="text-right graph-four-ctn"><i class="fa fa-level-up" aria-hidden="true"></i> <span class="counter text-danger"><span class="counter"> @if($farms->max('ph') > $ph->maximum) {{$farms->max('ph')}} @endif</span></span>
                                </li>
                                <li> @if($farms->min('ph') < $ph->minimum) {{$farms->where('ph', $farms->min('ph'))->first()->farm->name}} @endif</li>
                                <li class="text-right graph-four-ctn"><i class="fa fa-level-down" aria-hidden="true"></i> <span class="text-danger"><span class="counter"> @if($farms->min('ph') < $ph->minimum) {{$farms->min('ph')}} @endif</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info-cs res-mg-t-30 table-mg-t-pro-n">
                            <h3 class="box-title">Suhu Tambak</h3>
                            <ul class="list-inline two-part-sp">
                                <li>  @if($farms->max('temp') > $temp->maximum) {{$farms->where('temp', $farms->max('temp'))->first()->farm->name}} @endif </li>
                                <li class="text-right graph-four-ctn"><i class="fa fa-level-up" aria-hidden="true"></i> <span class="counter text-danger"><span class="counter"> @if($farms->max('temp') > $temp->maximum) {{$farms->max('temp')}} @endif</span></span>C
                                </li>
                                <li> @if($farms->min('temp') < $temp->minimum) {{$farms->where('temp', $farms->min('temp'))->first()->farm->name}} @endif</li>
                                <li class="text-right graph-four-ctn"><i class="fa fa-level-down" aria-hidden="true"></i> <span class="text-danger"><span class="counter"> @if($farms->min('temp') < $temp->minimum) {{$farms->min('temp')}} @endif</span></span>C
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info-cs res-mg-t-30 res-tablet-mg-t-30 dk-res-t-pro-30">
                            <h3 class="box-title">Dissolved Oxygen Tambak</h3>
                            <ul class="list-inline two-part-sp">
                                <li> @if($farms->max('do') > $do->maximum) {{$farms->where('do', $farms->max('do'))->first()->farm->name}} @endif </li>
                                <li class="text-right graph-four-ctn"><i class="fa fa-level-up" aria-hidden="true"></i> <span class="counter text-danger"><span class="counter"> @if($farms->max('do') > $do->maximum) {{$farms->max('do')}} @endif</span></span> ppm
                                </li>
                                <li>@if($farms->min('do') < $do->minimum) {{$farms->where('do', $farms->min('do'))->first()->farm->name}} @endif </li>
                                <li class="text-right graph-four-ctn"><i class="fa fa-level-down" aria-hidden="true"></i> <span class="text-danger"><span class="counter"> @if($farms->min('do') < $do->minimum) {{$farms->min('do')}} @endif</span></span> ppm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="program-widget-bc mg-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    @foreach ($locations as $location)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="hpanel shadow-inner responsive-mg-b-30" style="margin-top: 20px">
                            <div class="panel-body">
                                <div class="table-responsive wd-tb-cr">
                                    <div class="main-sparkline8-hd">
                                        <h1>{{$location->name}}</h1>
                                    </div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tambak</th>
                                                <th>ph</th>
                                                <th>suhu</th>
                                                <th>do</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($location->farms as $farm)
                                            <tr>
                                                <td>
                                                    <form action="/tambak/" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="location" value="{{$location->name}}">
                                                        <input type="hidden" name="tambak" value="{{$farm->name}}">
                                                        <button type="submit" class="btn btn-default">
                                                            <span class="">{{$farm->name}}</span>
                                                        </button>
                                                    </form>
                                                </td>
                                                <td>
                                                    <span @if($farms->where('farm_id', $farm->id)->avg('ph') >= $ph->minimum && $farms->where('farm_id', $farm->id)->avg('ph') <= $ph->maximum) style="color: green" @else style="color: red" @endif >
                                                        {{round($farms->where('farm_id', $farm->id)->avg('ph'))}}
                                                    </span>
                                                </td>
                                                <td>
                                                    <span @if($farms->where('farm_id', $farm->id)->avg('temp') >= $temp->minimum && $farms->where('farm_id', $farm->id)->avg('temp') <= $temp->maximum) style="color: green" @else style="color: red" @endif >
                                                        {{round($farms->where('farm_id', $farm->id)->avg('temp'))}}
                                                    </span>
                                                </td>
                                                <td>
                                                    <span @if($farms->where('farm_id', $farm->id)->avg('do') >= $do->minimum && $farms->where('farm_id', $farm->id)->avg('do') <= $do->maximum) style="color: green" @else style="color: red" @endif >
                                                        {{round($farms->where('farm_id', $farm->id)->avg('do'))}}
                                                    </span>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

@endsection

@section('script')

@endsection