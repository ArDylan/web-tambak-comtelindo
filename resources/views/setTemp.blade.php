@extends('layouts.app')
@section('content')


        {{-- statistik atas --}}
        <div class="traffice-source-area mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    @foreach ($params as $param)
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info-cs">
                            <h3 class="box-title">Pengaturan {{$param->name}}</h3>
                            <form action="/atur-suhu/{{$param->id}}" method="POST">
                                @method('patch')
                                @csrf
                                <ul class="list-inline two-part-sp">
                                    <li>Minimum</li>
                                    <li class="text-right sp-cn-r">
                                        <div class="form-select-list">
                                            <input type="text" class="form-control" name="minimum" value={{$param->minimum}}>
                                        </div>
                                    </li>
                                    <li></li>
                                    <li></li>
                                    <li>Maximum</li>
                                    <li class="text-right graph-four-ctn">
                                        <div class="form-select-list">
                                            <input type="text" class="form-control" name="maximum" value="{{$param->maximum}}">
                                        </div>
                                    </li>
                                    <div style="float: right; margin-top: 20px; margin-right: 10px">
                                        <button class=" btn btn-sm btn-primary">Simpan</button>
                                    </div>
                                </ul>
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
@endsection
