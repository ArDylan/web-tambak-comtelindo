@extends('layouts.app')
@section('content')


    {{-- statistik atas --}}
    <div class="traffice-source-area mg-b-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="white-box analytics-info-cs">
                        <h3 class="box-title">Cari Tambak</h3>
                        <form action="/tambak/" method="POST">
                            @csrf
                            <div class="sparkline10-graph">
                                <div class="input-knob-dial-wrap">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="chosen-select-single mg-b-20">
                                                <label>Lokasi</label>
                                                <select data-placeholder="Choose a Country..." class="chosen-select" tabindex="-1" name="location">
                                                    @foreach ($locations as $location)
                                                        <option value="{{$location->name}}">{{$location->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class=" mg-b-20">
                                                <label>Tambak</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Tambak" name="tambak"/>
                                            </div>
                                            <div class=" mg-b-20">
                                                <button type="submit" class="btn btn-primary">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@if ($searchCheck != NULL)
    {{-- statistik ph --}}
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sparkline-list">
                        <div class="smart-sparkline-hd">
                            <div class="smart-main-spark-hd">
                                <h1>PH TAMBAK {{$farmName}}</h1>
                            </div>
                        </div>
                        <div class="smart-sparkline-list">
                            @if($farms->count() > 0)
                            <div id="phChart"></div>
                            @else
                            <div>Tidak Ada Data Hari Ini</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- statistik suhu --}}
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sparkline-list">
                        <div class="smart-sparkline-hd">
                            <div class="smart-main-spark-hd">
                                <h1>SUHU TAMBAK {{$farmName}}</h1>
                            </div>
                        </div>
                        <div class="smart-sparkline-list">
                            @if($farms->count() > 0)
                            <div id="suhuChart"></div>
                            @else
                            <div>Tidak Ada Data Hari Ini</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- statistik oksigen --}}
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sparkline-list">
                        <div class="smart-sparkline-hd">
                            <div class="smart-main-spark-hd">
                                <h1>DO TAMBAK {{$farmName}}</h1>
                            </div>
                        </div>
                        <div class="smart-sparkline-list">
                            @if($farms->count() > 0)
                            <div id="doChart"></div>
                            @else
                            <div>Tidak Ada Data Hari Ini</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sparkline-list">
                        <div class="smart-sparkline-hd">
                            <div class="smart-main-spark-hd">
                                <h1>Silahkan memasukkan lokasi dan tambak yang diinginkan</h1>
                            </div>
                        </div>
                        <div class="smart-sparkline-list">
                            {{-- <div>Tidak Ada Data Hari Ini</div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@endsection

@section('script')

<script>
   google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(phChart);
      google.charts.setOnLoadCallback(suhuChart);
      google.charts.setOnLoadCallback(doChart);

    function phChart() {

        var ph = google.visualization.arrayToDataTable([
            ['hours', 'ph', {role: 'style'}],
            @foreach ($farms as $farm)
            ['{{$farm->time}}', {{$farm->ph}},
            @if($farm->ph >= $ph->minimum && $farm->ph <= $ph->maximum)
            'blue'
            @else
            'red'
            @endif
            ],
            @endforeach
        ]);

        var options = {
          title: 'PH',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

      var chart = new google.visualization.LineChart(document.getElementById('phChart'));

      chart.draw(ph, options);
    }

    function suhuChart() {
        var suhu = google.visualization.arrayToDataTable([
            ['hours', 'suhu', {role: 'style'}],
            @foreach ($farms as $farm)
            ['{{$farm->time}}', {{$farm->temp}},
            @if($farm->temp >= $temp->minimum && $farm->temp <= $temp->maximum)
            'blue'
            @else
            'red'
            @endif
            ],
            @endforeach
        ]);

        var options = {
          title: 'SUHU',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

      var chart = new google.visualization.LineChart(document.getElementById('suhuChart'));

      chart.draw(suhu, options);
    }

    function doChart() {
        var oxygen = google.visualization.arrayToDataTable([
            ['hours', 'dissolved oxygen', { role: 'style'}],
            @foreach ($farms as $farm)
            ['{{$farm->time}}', {{$farm->do}}, 
                @if($farm->do >= $do->minimum && $farm->do <= $do->maximum)
                'blue'
                @else
                'red'
                @endif
            ],
            @endforeach
        ]);

        var options = {
          title: 'Dissolved Oxygen',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

      var chart = new google.visualization.LineChart(document.getElementById('doChart'));

      chart.draw(oxygen, options);
    }

</script>

@endsection