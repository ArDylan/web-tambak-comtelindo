@extends('layouts.app')
@section('content')
        <!-- Static Table Start -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Tabel Tambak</h1>
                                </div>
                            </div>
                            <!-- Multi Upload Start-->
                            {{-- <div class="multi-uploaded-area mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="dropzone-pro mg-tb-30">
                                                <div id="dropzone1" class="multi-uploader-cs">
                                                    <form action="/riwayat/import" class="dropzone dropzone-custom needsclick" id="demo1-upload" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="dz-message needsclick download-custom">
                                                            <i class="fa fa-download" aria-hidden="true"></i>
                                                            <h2>Drop files here or click to upload.</h2>
                                                            <p><span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary">IMPORT</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <!-- Multi Upload End-->
                            <form action="/riwayat/import" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <input name="excel" class="form-control" type="file" required>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <button class="btn btn-primary">Import</button>
                                    </div>
                                </div>
                            </form>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <table id="table-history" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="sorting" data-field="name" data-editable="true">Lokasi</th>
                                                <th class="sorting" data-field="name" data-editable="true">Tambak</th>
                                                <th class="sorting" data-field="email" data-editable="true">Detail</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
@endsection


@section('script')

<script>

    $.ajaxSetup({
        headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content"),
            }
        })

</script>

<script>

$(document).ready(function(){
    var historyTable =  $('#table-history').DataTable({
    processing: true,
    serverSide: true,
    destroy: true,
    responsive: true,
    ajax: {
        type: "GET",
        url: "{{route('history')}}",
    },
    columns: [
        { data: 'location', name: 'location'},
        { data: 'name', name: 'name'},
        { data: 'action', name: 'action'},
    ],
    });
});

</script>

@endsection