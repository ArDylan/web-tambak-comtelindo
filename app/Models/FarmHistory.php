<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FarmHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'ph',
        'do',
        'temp',
        'date',
        'time',
        'farm_id',
    ];

    public function farm()
    {
        return $this->belongsTo('App\Models\Farm');
    }
}
