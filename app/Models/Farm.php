<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    use HasFactory;

    public function farmHistories()
    {
        return $this->hasMany('App\Models\FarmHistory');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }
}
