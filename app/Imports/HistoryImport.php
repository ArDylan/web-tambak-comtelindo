<?php

namespace App\Imports;

use App\Models\FarmHistory;
use App\Models\Farm;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class HistoryImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $farm = Farm::where('name', $row['tambak'])->first();
        $farmHistory = FarmHistory::where('farm_id', $farm->id)->where('date', $row['tanggal'])->get();
            if($farmHistory == NULL || $farmHistory->where('time', $row['jam'])->first() == NULL){
          	return new FarmHistory([
                'ph' => $row['ph'],
                'do' => $row['do'],
                'temp' => $row['temperatur'],
                'date' => $row['tanggal'],
                'time' => $row['jam'],
                'farm_id' => $farm->id,
            ]);
        };
    }
}
