<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Exports\HistoryExport;
use App\Imports\HistoryImport;
use Excel;

use App\Models\FarmHistory;
use App\Models\Location;
use App\Models\Farm;
use App\Models\Parameter;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;




class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nowDate = Carbon::now()->format('Y-m-d H:i');
        $nowDate = Carbon::parse($nowDate)->add(8, 'hour');
        $nowDate = $nowDate->format("Y-m-d");
        $farms = FarmHistory::where('date', $nowDate)->get();
        $locations = Location::all();
        // dump($nowDate);
        $parameters = Parameter::all();

        $ph = $parameters->where('name', 'ph')->first();
        $temp = $parameters->where('name', 'temperatur')->first();
        $do = $parameters->where('name', 'do')->first();

        // $avgPh = $farms;
         
        // dd($farms);
        
        return view('index', compact('farms','locations', 'ph', 'temp', 'do', 'nowDate'));
        
    }

    public function test(Request $request)
    {
        $nowDate = Carbon::now()->format('Y-m-d H:i');
        $nowDate = Carbon::parse($nowDate)->add(8, 'hour');
        $nowDate = $nowDate->format("Y-m-d");
        $farms = Farm::all();
        $parameters = Parameter::all();
        $ph = $parameters->where('name', 'ph')->first();
        $temp = $parameters->where('name', 'temperatur')->first();
        $do = $parameters->where('name', 'do')->first();

        $dPh = [];
        $dTemp = [];
        $dDo = [];
        foreach($farms as $farm){
            $farmHistory = FarmHistory::where('farm_id', $farm->id)->where('date', $nowDate)->orderBy('time', 'ASC')->get();
            $farmPH = $farmHistory->whereBetween('ph', [$ph->minimum, $ph->maximum]);


            // dump($farmPH); 
            array_push($dPh, $farmPH);
            // array_push($dTemp, $tempF);
            // array_push($dDo, $doF);
        }


        // $avgPh = $farms;
         

        // dump(collect($dPh));
        // dump(collect($dTemp));
        dd(collect($dPh)->count());
        return view('index', compact('farms', 'ph', 'temp', 'do'));
        
    }

    public function farms(Request $request)
    {
        $nowDate = Carbon::now()->format('Y-m-d H:i');
        $nowDate = Carbon::parse($nowDate)->add(8, 'hour');
        $nowDate = $nowDate->format("Y-m-d");
        $farms = collect([]);
        $searchCheck = null;
        // dump($nowDate);
        $locations = Location::all();
        $parameters = Parameter::all();

        $ph = $parameters->where('name', 'ph')->first();
        $temp = $parameters->where('name', 'temperatur')->first();
        $do = $parameters->where('name', 'do')->first();

        // $avgPh = $farms;
         
        // dd($farms);
        return view('farmSite', compact('farms', 'searchCheck','locations', 'ph', 'temp', 'do'));
        
    }

    
    public function search(Request $request)
    {        
        $locations = Location::all();
        $parameters = Parameter::all();
        $lokasi = $locations->where('name', $request->location)->first();
        $nowDate = Carbon::now()->format('Y-m-d H:i');
        $nowDate = Carbon::parse($nowDate)->add(8, 'hour');
        $nowDate = $nowDate->format("Y-m-d");
        // dd($nowDate->format("Y-m-d"));
        if($request->tambak != NULL){
            $searchCheck = 1;
            $farm =Farm::where('location_id', $lokasi->id)->where('name', $request->tambak)->first();
            $farmName = ucWords($farm->name);
            $farms = FarmHistory::where('date', $nowDate)->where('farm_id', $farm->id)->orderBy('time', 'ASC')->get();
        }else{
            $searchCheck = NULL;
            $farm = NULL;
            $farmName = "-";
            $farms = collect([]);
        }
        $ph = $parameters->where('name', 'ph')->first();
        $temp = $parameters->where('name', 'temperatur')->first();
        $do = $parameters->where('name', 'do')->first();

        return view('farmSite', compact('farms','farmName', 'searchCheck','locations', 'ph', 'temp', 'do'));
        
    }

    public function setTemp()
    {
        $params =  Parameter::all();
        return view('setTemp', compact('params'));
    }

    public function updateTemp(Request $request, Parameter $parameter)
    {
        $parameter->update([
            'minimum' => $request->minimum,
            'maximum' => $request->maximum,
        ]);

        return redirect('/atur-suhu');
    }

    public function history(Request $request)
    {
        $farms = Farm::all();

        if($request->ajax())
        {
            return DataTables::of($farms)
            ->addColumn('location', function($farms){
                $location = $farms->location->name;
                return $location;
            })
            ->addColumn('action', function($farms){
                $button = "
                <div>
                    <a href=\"/riwayat/$farms->id\" class=\"btn btn-primary\" style=\"color: white\">Detail</a>
                </div>";
                return $button;
            })
            ->rawColumns(['location', 'action'])
            ->make(true);
        }
        // dd($farms);

        return view('history');
        
    }

    public function detailHistory(Request $request, $farmHistory)
    {
        // dd($farmHistory);
        if($request->ajax())
        {          
            $detailHistory = FarmHistory::where('farm_id', $farmHistory)->get();
            // dd($detailHistory);
            if ($request->data1 != null || $request->data2 != null) {
                $detailHistory = $detailHistory->whereBetween('date', [Carbon::parse($request->data1)->format('Y-m-d'), Carbon::parse($request->data2)->format('Y-m-d')]);
            }

            return DataTables::of($detailHistory)
            ->addIndexColumn()
            ->make(true);
        }
        // dd($request);
        $tanggal1 = null;
        $tanggal2 = null;
        $farm = Farm::where('id', $farmHistory)->first();

        return view('detailHistory', compact('tanggal1', 'tanggal2', 'farm'));
        
    }

    public function export(){
        return Excel::download(new HistoryExport(), 'REPORT.xlsx');
    }

    public function import(Request $request)
    {
        $file = $request->file('excel');
        // dd($file);
        Excel::import(new HistoryImport, $file);
        return redirect('/riwayat');
    }

}
