<?php
namespace App\Exports;
use App\Models\Farm;
use Carbon\Carbon;
use App\Models\FarmHistory;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

class HistoryExport implements Fromcollection, WithHeadings, ShouldAutoSize, WithMapping, WithEvents
{
    use Exportable;
    public function headings():array
    {
        return [
            'Lokasi',
            'Tambak',
            'Tanggal',
            'Jam',
            'PH',
            'Temperatur',
            'DO',
        ];
    }

    public function collection()
    {
        $farm_id = $_POST['farm_id'];
        $detailHistory = FarmHistory::where('farm_id', $farm_id)->orderBy('time', 'ASC')->get();

        if($_POST['startDate'] != NULL || $_POST['endDate'] != NULL){
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
            $detailHistory = $detailHistory->whereBetween('date', [Carbon::parse($startDate)->format('Y-m-d'), Carbon::parse($endDate)->format('Y-m-d')]);
        }

        return collect($detailHistory);
    }

    public function map($detailHistory):array
    {
        $location = $detailHistory->farm->location->name;
        $farm = $detailHistory->farm->name;
        $date = $detailHistory->date;
        $time = $detailHistory->time;
        $ph = $detailHistory->ph;
        $temp = $detailHistory->temp;
        $do = $detailHistory->do;
        return [
            $location,
            $farm,
            $date,
            $time,
            $ph,
            $temp,
            $do,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->getStyle('A1:G1')->applyFromArray([

                    'font' => [
                        'bold' => true
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => 'FFFF00',
                        ],
                        'endColor' => [
                            'argb' => 'FFFF00',
                        ],
                    ],
                ]);
            }
        ];
    }

}
