<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/tambak', 'HomeController@farms');
Route::post('/tambak', 'HomeController@search');
Route::get('/atur-suhu', 'HomeController@setTemp');
Route::patch('/atur-suhu/{parameter}', 'HomeController@updateTemp');
Route::get('/riwayat', 'HomeController@history')->name('history');
Route::post('/riwayat/export', 'HomeController@export');
Route::post('/riwayat/import', 'HomeController@import');
Route::get('/riwayat/{farmHistory}', 'HomeController@detailHistory')->name('detail.history');

// Route::get('/', 'HomeController@index');

// // Route Admin
// Route::get('/admin', 'AdminController@index')->name('admin');
// Route::post('/admin/export', 'AdminController@export')->name('admin.export');
// Route::get('/admin/user-management', 'AdminController@management')->name('management');
// Route::get('/admin/user-management/edit/{user}', 'AdminController@edit');
// Route::patch('/admin/user-management/{user}', 'AdminController@update');
// Route::delete('/admin/user-management/{user}', 'AdminController@destroy');
// Route::get('/admin/list-job','AdminController@listJob')->name('list-job');
// Route::post('/admin/list-job/create','AdminController@createJob')->name('create.job');
// Route::delete('/admin/list-job/{listJob}', 'AdminController@destroyJob');
// Route::get('/admin/list-job/edit/{listJob}', 'AdminController@editJob')->name('edit.job');
// Route::patch('/admin/list-job/{listJob}', 'AdminController@updateJob')->name('update.job');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
